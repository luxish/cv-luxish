import * as React from "react";
import ReactDOM from "react-dom";
import WhoAmI from "./sections/whoami/whoami";
import Career from "./sections/career/career";
import Education from "./sections/education/education";
import "./app.css";
import Interests from "./sections/interests/interests";
import Footer from "./sections/footer/footer";

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = { "data" : {} };
    }

    componentDidMount() {
        fetch('static/data.json')
            .then(response => response.json())
            .then( data => {
                this.setState( {data} );
            });
    }

    render() {
        const personalDetails = this.state.data.personalDetails;
        const career = this.state.data.career;
        const education = this.state.data.education;
        const interests = this.state.data.interests;;
        return (
            <React.Fragment>
                <WhoAmI personalDetails={personalDetails} />
                <Career careerData={career} />
                <Education education={education} />
                <Interests interests={interests} />
                <Footer />
            </React.Fragment>
        );
    }
} 

ReactDOM.render(<App />, document.querySelector("#appRoot"));