import * as React from "react"; 

export default class List extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const title = this.props.title;
        const children = this.props.children ? this.props.children : [];
        const rootClasses = "list " + this.props.className;
        return (
            <div className={rootClasses}>
                <div className="list-title">{title}</div>
                {children.map(el => 
                    <div className="list-item" key={el.title ? el.title : el.key }>
                        {el}
                    </div>
                )}
            </div>
        )
    }
}