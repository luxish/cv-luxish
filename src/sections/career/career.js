import * as React from "react";
import "./career.css"
import List from "../../shared/list-view";

const ListItem = (props) => {
    return (
        <React.Fragment>
            <div className="position">{props.value.position}</div>
            <div className="duration">{props.value.duration}</div>
            {props.value.projects.map(item => 
                    <div key={item.id} className="project">
                            <div className="title">{item ? item.title : ""}</div>
                            <div className="description">
                                {item ? item.description : ""}
                            </div>
                            <div className="description">
                                {item ? "Technology stack: " + item.techstack : ""}
                            </div>
                    </div>
                )}
        </React.Fragment>
    );
}

export default class Career extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        const careerlist = this.props.careerData ? this.props.careerData : [];
        return (
           <List className="career-section" title={"Work experience"}>
                  {careerlist.map(el => <ListItem key={el.position} value={el} />)}
           </List>
        )
    }
}