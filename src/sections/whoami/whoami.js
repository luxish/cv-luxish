import React from 'react';
import './whoami.css';

const Details = (props) => {
    return (
        <React.Fragment>
            <div className="details-item">Age: {props.details ? props.details.age : ""}</div>
            <div className="details-item">From: {props.details ? props.details.from: ""}</div>
            <div className="details-item">Languages: {props.details ? props.details.languages : ""}
            </div>
            <div className="details-item">Email: {props.details ? props.details.email : ""}</div>
        </React.Fragment>
    )
}

export default class WhoAmI extends React.Component {
    
    constructor(props) {
        super(props);
    }
    
    render() {
        const personalDetails = this.props.personalDetails;
        return (
            <div className="lux-header-content">
                <div className="lux-header">
                    <div className="lux-avatar">
                        { personalDetails && personalDetails.avatarData ? <img src={personalDetails.avatarData} />  : <span>N/A</span> }
                    </div>
                    <div className="lux-details">
                        <div className="lux-name">{ personalDetails && personalDetails.name ? personalDetails.name : "" }</div>
                        <div className="lux-base-details">
                            <Details details={personalDetails && personalDetails.details ? personalDetails.details : []} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}