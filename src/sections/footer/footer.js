import * as React from "react";
import "./footer.css";
export default class Footer extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="footer">
                <div> Created with <a href="https://reactjs.org/">ReactJs </a> </div>
                <div> Repository: <a href="https://gitlab.com/luxish/cv-luxish">https://gitlab.com/luxish/cv-luxish</a></div>
            </div>
        )
    }
}