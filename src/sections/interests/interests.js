import * as React from "react";
import "./interests.css"
import List from "../../shared/list-view";

const ListItem = (props) => {
    return (
        <div className="interests-paragraph">
            <div className="paragraph">{props.value}</div>
        </div>
    );
}

export default class Interests extends React.Component {

    constructor(props) {
        super(props);
    }


    createId(content) {
        return content.substring(0, 10) ;
    }

    render(){
        const interests = this.props.interests ? this.props.interests : [];
        const title = "Interests";
        return (
            <List className="interests-section" title={title}>
                {interests.map(el => <ListItem key={this.createId(el)} value={el} />)}
            </List>
        )
    }
}