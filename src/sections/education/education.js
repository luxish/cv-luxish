import * as React from "react";
import "./education.css"
import List from "../../shared/list-view";

const ListItem = (props) => {
    return (
        <div className="education-paragraph">
            <div className="title">{props.value.title}</div>
            <div className="description">{props.value.description}</div>
        </div>
    );
}

export default class Education extends React.Component {

    constructor(props) {
        super(props);
    }

    render(){
        const education = this.props.education ? this.props.education : [];
        const title = "Education";
        return (
            <List className="education-section" title={title}>
                {education.map(el => <ListItem key={el.title} value={el} />)}
            </List>
        )
    }
}